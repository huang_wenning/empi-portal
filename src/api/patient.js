import service from '@/utils/request'

const path = '/patient'

const patient = {}

patient.getList = function(data) {
  return service.post(path + '/getList', data)
}

patient.getRelList = function(data) {
  return service.get(path + '/getRelList/' + data)
}

export { patient }

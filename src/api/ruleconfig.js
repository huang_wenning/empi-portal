import service from '@/utils/request'

const path = '/weightConfig'

export function getAllRuleList() {
  return service.get(path + '/getAll')
}

export function saveRule(data) {
  return service.post(path + '/save', data)
}

export function delRule(data) {
  return service.delete(path + '/delete/' + data)
}

export function saveRuleField(data){
  return service.post(path + '/saveField', data)
}

export function delRuleField(data) {
  return service.delete(path + '/deleteField/' + data)
}

import service from '@/utils/request'

const path = '/callable'

export function getMergeLog(data) {
  return service.get(path + '/getMergeLog/' + data)
}

export function getMergeInfo() {
  return service.get(path + '/getMergeInfo')
}

export function propAutoMerge(data) {
  return service.get(path + '/propAutoMerge/' + data, { timeout: 0 })
}

export function getUnMergeCount(){
  return service.get(path + '/getUnMergeCount')
}

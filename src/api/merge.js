import service from '@/utils/request'

const path = '/patient'

const merge = {}

merge.getMergeList = function(data) {
  return service.post(path + '/getMergeList', data)
}

merge.getRelList = function(data) {
  return service.post(path + '/getRelList/' , data)
}

merge.getCompareList = function(data) {
  return service.get(path + '/getCompareList/'+ data)
}

merge.mergeRow = function(data){
  return service.get(path + '/mergeRow/'+ data)
}

merge.spiltRow = function(data){
  return service.get(path + '/spiltRow/'+ data)
}

export { merge }

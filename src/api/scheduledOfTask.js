import service from '@/utils/request'

const path = '/scheduledTask'

const obj = {}

/**
 * 查看任务列表
 */
obj.getTaskList = function(data) {
  return service.get(path + '/getTaskList')
}

/**
 * 修改、新增任务
 * @param data
 * @returns {AxiosPromise<any>}
 */
obj.saveTask = function(data) {
  return service.post(path + '/saveTask/' , data)
}


export { obj }

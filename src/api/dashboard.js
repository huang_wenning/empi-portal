import service from '@/utils/request'

const option =
  {
    'title': { 'x': 'left' ,text:'主索引趋势'},
    'tooltip': { 'trigger': 'axis' },
    'legend': { 'selected': { '总合并数': false, '总拆分数': false, '总新增数': false } },
    'grid': { 'left': '2%', 'right': '1.5%', 'bottom': '3%', 'containLabel': true },
    'toolbox': {
      'show': false,
      'feature': {
        'mark': { 'show': true },
        'dataView': { 'show': true, 'readOnly': false },
        'magicType': { 'show': true, 'type': ['line', 'bar', 'stack', 'tiled'] },
        'restore': { 'show': true },
        'saveAsImage': { 'show': true }
      }
    },
    'yAxis': [{ 'type': 'value' }],
    'series': [{
      'name': '合并数',
      'type': 'line',
      'smooth': true,
      'itemStyle': { 'normal': { 'color': 'Red', 'lineStyle': { 'color': 'Red' } } },
      'data': [8, 5, 3, 0, 10920, 49, 53, 8, 82, 19, 9, 81, 74, 45, 45, 34, 14, 18, 22, 4, 0, 0, 0, 1, 0, 1, 20, 95, 137, 118]
    }, {
      'name': '拆分数',
      'smooth': true,
      'type': 'line',
      'itemStyle': { 'normal': { 'color': 'blue', 'lineStyle': { 'color': 'blue' } } },
      'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }, {
      'name': '新增数',
      'type': 'line',
      'smooth': true,
      'itemStyle': { 'normal': { 'color': '#3eade2', 'lineStyle': { 'color': '#3eade2' } } },
      'data': [128, 87, 41, 1, 93502, 228, 184302, 77, 362, 93, 2, 409, 214, 232, 189, 185, 71, 175, 142, 52, 35, 2, 1, 2, 3, 8, 155, 446, 543, 328]
    }, {
      'name': '总合并数',
      'type': 'line',
      'smooth': true,
      'itemStyle': { 'normal': { 'color': '#96088e', 'lineStyle': { 'color': '#96088e' } } },
      'data': [66455, 66460, 66463, 66463, 77383, 77432, 77485, 77493, 77575, 77594, 77603, 77684, 77758, 77803, 77848, 77882, 77896, 77914, 77936, 77940, 77940, 77940, 77940, 77941, 77941, 77942, 77962, 78057, 78194, 78312]
    }, {
      'name': '总拆分数',
      'type': 'line',
      'smooth': true,
      'itemStyle': { 'normal': { 'color': '#f3ad06', 'lineStyle': { 'color': '#f3ad06' } } },
      'data': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    }, {
      'name': '总新增数',
      'type': 'line',
      'smooth': true,
      'itemStyle': { 'normal': { 'color': 'green', 'lineStyle': { 'color': 'green' } } },
      'data': [238867, 238954, 238995, 238996, 332498, 332726, 517028, 517105, 517467, 517560, 517562, 517971, 518185, 518417, 518606, 518791, 518862, 519037, 519179, 519231, 519266, 519268, 519269, 519271, 519274, 519282, 519437, 519883, 520426, 520754]
    }],
    'xAxis': {
      'type': 'category',
      'data': ['2021-01-21', '2021-01-22', '2021-01-23', '2021-01-24', '2021-01-25', '2021-01-26', '2021-01-27', '2021-01-28', '2021-01-29', '2021-01-30', '2021-01-31', '2021-02-01', '2021-02-02', '2021-02-03', '2021-02-04', '2021-02-05', '2021-02-06', '2021-02-07', '2021-02-08', '2021-02-09', '2021-02-10', '2021-02-11', '2021-02-12', '2021-02-13', '2021-02-14', '2021-02-15', '2021-02-16', '2021-02-17', '2021-02-18', '2021-02-19']
    }
  }

const dashboard = {}

dashboard.getOption = function() {
  return option
}

export { dashboard }

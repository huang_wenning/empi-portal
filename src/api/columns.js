import service from '@/utils/request'

const path = '/columns'

export function getAllColumn() {
  return service.get(path + '/getAll')
}
